function calculate(num1, num2, callBackFunction) {
    let result = num1 * num1 + num2 * num2 + callBackFunction(num1, num2);
    return result;
}

function  calcProduct(num1, num2) {
    return num1 * num2;
}

function callSum(num1, num2) {
    return num1 + num2;
}

// Result = 325
console.log(calculate(5, 15, calcProduct));
// Result = 270
console.log(calculate(5, 15, callSum));