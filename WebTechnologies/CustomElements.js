class PopUpInfo extends HTMLParagraphElement {
    constructor() {
        // Always call super() first in constructor
        super();
        /*
        ** Element functionality defined in here
         */
        // First we attach a shadow root to this custom element
        var shadow = this.attachShadow({mode: "open"});

        // Then we use some DOM manipulation to create the element's internal shadow DOM structure
        // Create spans
        var wrapper = document.createElement('span');
        wrapper.setAttribute('class', 'wrapper');
        var icon = document.createElement('span');
        icon.setAttribute('class', 'icon');
        icon.setAttribute('tabindex', '0');
        var info = document.createElement('span');
        info.setAttribute('class', 'info');

        // Take attribute content and put it inside the info span
        var text = this.getAttribute("text");
        info.textContent = text;

        // Insert icon
        var imgURL;
        if (this.hasAttribute('img')) {
            imgURL = this.getAttribute('img');
        } else {
            imgURL = 'img/default.png';
        }
        var img = document.createElement('img');
        img.src = imgURL;
        icon.appendChild(img);

        // attach some CSS to the shadow root to style it
        var style = document.createElement('style');
        style.textContent = '.wrapper {' +
            'color: red;' +
            '}';
        // attach the created elements to the shadow dom
        shadow.appendChild(style);
        shadow.appendChild(wrapper);
        wrapper.appendChild(icon);
        wrapper.appendChild(info);
    }
}

// Finally, we  define our new element and
// register our custom element on the CustomElementRegistry
customElements.define('popup-info', PopUpInfo);